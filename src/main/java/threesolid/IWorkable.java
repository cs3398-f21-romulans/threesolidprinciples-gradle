package threesolid;

public interface IWorkable{
    public String work();
}
