package threesolid;

//IWorker Now implements both IWorkable and IFeedable
//Because A worker IS IWorkable, the "worker" variable will be an IWorkable.
//This allows extendable and will not break any dependant code
class Manager {
    IWorkable worker;

    public void Manager(){}

    public void setWorker(IWorkable w) {
        worker=w;
    }

    public void manage() {
        worker.work();
    }
}