package threesolid;

//SuperWorker only needs to use work() and eat(). Minimum responsibilities are used.

class SuperWorker implements IWorker{
    @Override
    public String work() {
        return "I'm a super worker!";
    }

    @Override
    public String eat() {
        return "I'm eating a super healthy meal";
    }
}