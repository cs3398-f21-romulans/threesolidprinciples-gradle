// This Worker class is a good example of the open close principle
// It allows for new functionality and it keeps existing code unmodified
// the worker class allows for get names, setting names, and printing messages
// all while the other class can't change the code. 
// Author: Jeffrey Pearce

package threesolid;

class Worker implements IWorker{
	private String name = "";

  	public String getName()
  	{
    	return name;
  	}

  	public void setName(String name)
  	{
      	this.name = name;
  	}

	@Override
  	public String work()
  	{

  		if (name == "")
    	{
       		return "I'm working already!";
    	}
    	else
    	{
       		return name + " is working very hard!";
    	}
	}

	@Override
	public String eat()
	{
		if (name == "")
    	{
       		return "I'm eating already!";
    	}
    	else
    	{
    		return name + " is eating a double cheeseburger with special sauce and bacon flavored Skittles!";
    	}
	}

      public Boolean addpositive(int a, int b)
    {
      if ((a+b) > 0)
	  {
        return(true);
	  }
	  else
	  {
      	return(false);
	  }
    }
}